---
layout: 2023/post
section: proposals
category: workshops
author: Elena Lopez de Arroiabe, Aitziber Ruiz, Joseba Mouriz, Maria Gutiérrez-Segú
title: Taller práctico de Arduino Educación
---

# Taller práctico de Arduino Educación

>En este taller llevaremos adelante una experiencia práctica usando Arduino Educación.  Haremos una actividad resumida en la que podamos ver cómo es la plataforma educativa en paralelo con el material físico. Se trata de una propuesta centrada en el uso de Arduino en el aula, en la que podremos tener acceso al punto de vista del alumnado y el profesorado.<br><br>
Cada pareja contará con un kit de Arduino Educación compuesto por una placa Arduino Uno, placa de prototipado y los elementos necesarios para llevar adelante la práctica. Cada participante tendrá acceso ilimitado a la plataforma educativa durante dos semanas.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>El taller en una mezcla teórico-práctica de 1h y 15 min de duración. Las personas asistentes podrán conocer la plataforma educativa y los diferentes materiales y cursos. La participación en el taller incluye el acceso ilimitado a la plataforma educativa durante dos semanas.<br><br>
La propuesta metodológica de Arduino Educación se centra en el trabajo en parejas o tríos. En el taller trabajaremos en parejas. Cada pareja debe disponer de un ordenador. Previo al taller, para agilizar el mismo y poder sacar el máximo provecho, se recomienda que las personas participantes se hagan un usuario Arduino (https://www.arduino.cc) y se descarguen el IDE de Arduino ( https://www.arduino.cc/en/software)

-   Web del proyecto: <https://www.tibot.es/content/10-que-es-arduino-education>

-   Público objetivo:

>Este taller está enfocado a profesorado de ESO, FP y Bachiller y personas de dirección. No es necesario tener experiencia ni conocimientos previos sobre Arduino y su programación ya que el enfoque se centra en el proceso de aprendizaje de ambos elementos.<br><br>
La plataforma educativa y el material son una oportunidad, tanto para el alumnado como para el profesorado y el centro, de llevar Arduino al aula de manera fácil y con continuidad.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 18:45 a 20:15 en el Aula 4 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/dfd2d6e6-416a-4225-a060-40d7a37dd0cd>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ponente - Elena Lopez de Arroiabe; Apoyo técnico - Aitziber Ruiz, Joseba Mouriz, Maria Gutiérrez-Segú.

-   Bio:

>El equipo de TIBOT lleva trabajando en robótica educativa más de 10 años. Desde hace 3 hemos empezado a impulsar la inclusión de Arduino Educación en centros de ESO, FP y Bachiller. Desde entonces realizamos múltiples talleres sobre este material y también formaciones concretas sobre este material u otros enfocado siempre a profesorado. Uno de nuestros pilares es el enfoque educativo de la robótica y su inclusión en el aula.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
