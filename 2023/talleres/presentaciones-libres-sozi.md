---
layout: 2023/post
section: proposals
category: workshops
author: Juan José Martín Romero
title: Presentaciones libres con Sozi
---

# Presentaciones libres con Sozi

>Taller para enseñar a utilizar Sozi, creada por Guillaume Savaton (<https://sozi.baierouge.fr/>). A partir de un dibujo SVG, creado por ejemplo a través de Inkscape, se creará una presentación que dará como resultado un archivo html, y por tanto reproducible en cualquier navegador moderno.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>1. Se comenzará creando un dibujo SVG a través de Inkscape. Con este dibujo crearemos una pequeña presentación utilizando Sozi.
2. Modificaremos el SVG para crear una presentación más compleja en la que e utilicen diferentes capas.
3. Avanzaremos en la complejidad introduciendo en la presentación links entre diapositivas, links con páginas web.
4. Por último, se mostrará como introducir archivos de audio y vídeo en la presentación.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 10:00 a 11:30 en el Aula 5 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/70c0552e-d1bd-4ffc-a849-9a6e87ded32f>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Juan José Martín Romero

-   Bio:

>Soy Dr. Ingeniero Industrial y mi labor profesional se desarrolla en el ámbito educativo como inspector de educación. He participado en el desarrollo de una herramienta libre, utilizando Django, para llevar a cabo la evaluación por competencias que determina la nueva Ley Orgánica de Educación.<br><br>
Entre el 2009 y el 2019 fui el responsable de organizar RiojaParty; un encuentro informático llevado a cabo en el instituto de Haro que acogía a más de 300 participantes venidos de toda España.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
