---
layout: 2023/post
section: proposals
category: workshops
author: Ana Mª López Floría, Carlos González Larraga, Daniel Esteban, Andrea Alfaro, Sergio Salgado
title: Mesa redonda&#58; “Digitalización en las aulas y oligopolios tecnológicos”
---

# Mesa redonda: “Digitalización en las aulas y oligopolios tecnológicos”

>Mesa redonda con participantes del profesorado, familias y administración educativa, donde debatir los modelos de digitalización del ámbito escolar que están llevando a cabo las administraciones educativas, con qué criterios y utilizando qué herramientas, cual es el sentir de la comunidad educativa al respecto y qué papel juega el software libre en todo ello como alternativa a los oligopolios tecnológicos.

## Detalles de la propuesta:

-   Tipo de propuesta: Mesa redonda
-   Idioma: Español

-   Descripción:

>Mesa redonda en la que intervienen:<br>
- Carlos González Larraga: profesor de tecnología y responsable COFOTAP del IES Miguel Catalán<br>
- Daniel Esteban: Asesor Técnico Docente Servicio de Plataformas Educativas de la Consejería de Educación de Madrid.<br>
- Andrea Alfaro: autora del libro Manual de supervivencia para madres y padres en tecnologías y redes sociales.<br>
- Sergio Salgado: Miembro del Instituto para la digitalización democrática - Xnet<br><br>
Modera: Ana Mª López Floría, profesora de tecnología y Asesora de CDD en el gobierno de Aragón.

-   Público objetivo:

>Profesorado, familias y administraciones educativas preocupadas por la forma en la que se está llevando a cabo la digitalización de las aulas y la alfabetización digital de la ciudadanía del mañana, y que quieren explorar alternativas posibles

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 13:00-14:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
