---
layout: 2023/post
section: proposals
category: workshops
author: Toni Moreno
title: Novedades ESP32 STEAMakers y ArduinoBlocks para el nuevo currículum
---

# Novedades ESP32 STEAMakers y ArduinoBlocks para el nuevo currículum

>ESP32 STEAMakers y ArduinoBlocks estan creados por docentes y probados dentro del aula.
El taller estará basado en diversas maquetas reales diseñadas para su uso en Tecnologia de ESO y Bachillerato, pero también para docentes de otras disciplinas STEAM. Trataremos de dar una visión muy práctica de algunos proyectos que pueden ser de interés

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Se mostraran en vivo diversos proyectos  basados en maquetas de aula con ESP32STEAMakers y ArduinoBlocks. Una primera línea de proyectos mostraran las posibilidades de trabajo con energías y eficiencia. Para ser eficientes hay que saber cuanta y cuando se gasta la energía.<br><br>
Se proponen proyectos donde se mide el consumo de energía mediante  ArduinoBlocks y ESP32STEAMakers y se programan pequeñas actividades que nos permitan disminuir el consumo. Se trabajan algunos ODS. Otra linia de proyectos muy actual y atractiva és la de realizar proyectos prácticos con IoT. Se mostraran algunos de estos proyectos utilizables en el aula en la línea del nuevo currículo.

-   Web del proyecto: <http://www.robolot.org>

-   Público objetivo:

>Docentes de Tecnología y de otras disciplinas STEAM.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 10:00 a 11:00 en el Aula 1 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/aca7e282-ef94-4712-8ff9-0f3dc35548a8>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Toni Moreno

-   Bio:

>Toni Moreno es el impulsor durante 22 ediciones (desde 2002) de ROBOLOT, actividad de difusión de la robótica didáctica y el trabajo con proyectos totalmente funcionales. Más de 30 años de docencia en automática y robótica y 6 años trabajando en Innovación Educativa. Es presidente de Robolot Team y formador de formadores en ICE UPC, ICE UdG y Robolot Team entre otros.

### Info personal:

-   Twitter: <https://twitter.com/ToniMorenoRey>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
