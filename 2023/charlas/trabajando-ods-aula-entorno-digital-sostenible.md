---
layout: 2023/post
section: proposals
category: talks
author: Stella Carrera Cuadrado y Mª Isabel Pareja Moreno
title: Trabajando los ODS en el aula con un entorno digital sostenible
---

# Trabajando los ODS en el aula con un entorno digital sostenible

>Propuesta formativa que hemos desarrollado para docentes, de cara a promover  la integración de los ODS en las situaciones de aprendizaje, con herramientas digitales de software libre en el entorno de MAX-Linux y Educamadrid.<br><br>
Estamos trabajando con los centros de formación del profesorado de la Comunidad de Madrid, desarrollando  propuestas didácticas y situaciones de aprendizaje que giran alrededor de los ODS, para que puedan ser aplicados en el aula,  a la vez que contribuimos a desarrollar su competencia digital de los docentes utilizando software libre, en concreto herramientas integradas en  el S.O. Max Linux y el entorno de Educamadrid.<br><br>
Nuestro objetivo es que la competencia digital vaya ligada a la sostenibilidad tecnológica, a la accesibilidad y al buen uso de las herramientas digitales.


## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En estas formaciones nos hemos centrado en que los docentes:
1. Conozcan los Objetivos de Desarrollo Sostenible (ODS) establecidos por la ONU
2. Sepan buscar y localizar herramientas digitales del S.O. Max-Linux como soporte para la enseñanza de los ODS. (eXeLearning, Audacity, Gimp, Inkscape, Scribus)
3. Creen actividades que faciliten el conocimiento de las ODS por parte del alumnado de niveles pre-universitarios
4. Compartan las propuestas didácticas con los demás en un  entorno seguro. (Cloud, Aulas virtuales y webs de EducaMadrid)

-   Público objetivo:

>Docentes de todos los niveles interesados conocer y/o desarrollar proyectos  educativos que integren los ODS en un entorno digital seguro y sostenible.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 11:00-11:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Stella Carrera Cuadrado, Mª Isabel Pareja Moreno

-   Bio:

>Somos docentes de diferentes niveles de enseñanzas pre-universitarias de la Comunidad de Madrid. Stella es Profesora de Secundaria de Tecnología y Mª Isabel es Maestra de Primaria especialidad Inglés.

### Info personal:

-   Web personal: <https://stellacarrera.wordpress.com/> / <https://www.educa2.madrid.org/web/maria.pareja>


## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
