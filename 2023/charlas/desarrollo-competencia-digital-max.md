---
layout: 2023/post
section: proposals
category: talks
author: Daniel Esteban Roque
title: Desarrollo de la competencia digital con MAX 11.5.1...¡sin conectarnos a Internet!
---

# Desarrollo de la competencia digital con MAX 11.5.1...¡sin conectarnos a Internet!

>MAX (MAdrid_linuX), la distribución educativa GNULinux de la Comunidad de Madrid, ha cumplido 18 años. Mostraremos todas las herramientas, programas y posibilidades que se incluyen en su versión 11.5.1 , para desarrollar la competencia digital. Lo más importante es que lo haremos sin conexión a Internet, por no disponer ni depender de esa conexión o, por no querer estar conectados constantemente.<br><br>
Esta decisión para desarrollar la competencia digital puede que no sea  la manera más "innovadora" en pleno 2023 pero estamos seguros de que es la más segura, libre y respetuosa con los usuarios.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Nos encontramos en un momento en que parece que el desarrollo de la competencia digital de nuestros alumnos y de nuestros docentes es algo que solo puede realizarse a través de Internet pero:<br><br>
- ¿Y si no disponemos de Internet? A veces la conexión falla en los centros o en las casas, incluso en el mercado existen equipos que, si no están conectados a Internet, apenas pueden realizar unas pocas tareas básicas, convirtiéndose en un elemento decorativo a la espera de que vuelva la conectividad. También puede darse una circunstancia más importante aún:<br><br>
- ¿Y si no queremos estar conectados a Internet mientras trabajamos en nuestro equipo? ¿y si queremos mantener la privacidad de nuestros proyectos y conectarnos solo si queremos subirlos o compartirlos en un espacio seguro como los que ofrece la plataforma EducaMadrid?<br><br>
Para todas esas situaciones la distribución MAX aparece como la solución perfecta y en media hora mostraremos algunas de las herramientas y aplicaciones ya instaladas para desarrollar las tareas cotidianas que suelen realizar alumnos y docentes.

-   Web del proyecto: <https://www.educa2.madrid.org/web/max/>

-   Público objetivo:

>Administración, docentes y equipos directivos en general.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 16:30-17:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Daniel Esteban Roque

-   Bio:

>Asesor Técnico Docente del Servicio de Plataformas Educativas de la Comunidad de Madrid. Actual coordinador del grupo MAX. Anteriormente he desarrollado mi trabajo como director del CRA Los Olivos, un centro rural que basaba su proyecto educativo, entre otros ejes, en el uso de del Software Libre y de EducaMadrid, la plataforma educativa, libre, segura y sostenible de la Comunidad de Madrid.

### Info personal:

-   Twitter: <https://twitter.com/@directorcra>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
