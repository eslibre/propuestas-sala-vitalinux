---
layout: 2023/post
section: proposals
category: talks
author: Pablo Garaizar Sagarminaga
title: La importancia del Software Libre en el ámbito escolar
---

# La importancia del Software Libre en el ámbito escolar

>En los últimos años, las administraciones públicas han fomentado la presencia de dispositivos y plataformas digitales en las escuelas y de aplicaciones ofimáticas o gestores de contenidos privativos ofrecidos como un servicio en la Nube. En esta charla analizaremos qué implicaciones tiene esta situación de muchos centros escolares en materia de software y qué alternativas propone el software libre para una alfabetización digital que respete los derechos digitales de estudiantes, docentes y familias.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remoto
-   Idioma: Español

-   Descripción:

>En los últimos años, la mayoría de centros educativos se enfrenta con escasos medios materiales y personales al reto de lograr una adecuada alfabetización digital. Desde las administraciones públicas, se presiona para aumentar la presencia de dispositivos y plataformas digitales en el ámbito escolar. Desde las empresas tecnológicas, se ofrecen aplicaciones ofimáticas y gestores de contenidos en la Nube con beneficios muy visibles y perjuicios más difíciles de identificar. Muchas familias creen que más tecnología implica un mejor aprendizaje y aceptan acríticamente esta cesión de derechos que va en contra de la soberanía digital escolar.<br><br>
En esta charla analizaremos qué implicaciones tiene la situación actual de muchos centros escolares en materia de software y qué alternativas propone el software libre para una alfabetización digital que respete la privacidad, reduzca la dependencia tecnológica, permita la interoperabilidad con otros sistemas y garantice el control de la información académica por parte de docentes, estudiantes y familias.

-   Público objetivo:

>Comunidad educativa: equipos directivos, docentes, estudiantes, familias.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 12:30-13:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Pablo Garaizar Sagarminaga

-   Bio:

>Pablo Garaizar es doctor en Ingeniería Informática y licenciado en Psicología. Trabaja como profesor titular en la Facultad de Ingeniería de la Universidad de Deusto donde imparte las asignaturas de Programación, Computación de Altas Prestaciones o Laboratorio de Tecnologías Emergentes. Colabora con la FSF desde hace más de 20 años y coordinó el grupo de interés en Software Libre de la Universidad de Deusto: e-ghost. Su ámbito de investigación se centra en el desarrollo del Pensamiento Computacional dentro del Deusto LearningLab. Además de haber contribuido al desarrollo de apps educativas libres como Social Lab, Kodetu, Make World o Lempel, es autor de juegos de mesa educativos libres como Moon, Arqueras de Nand, Block Miners o Red Team.

### Info personal:

-   Web personal: <http://garaizar.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@txipi>
-   Twitter: <https://twitter.com/@PGaraizar>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
