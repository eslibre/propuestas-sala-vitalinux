---
layout: 2023/post
section: proposals
category: talks
author: Héctor Banzo, Paola García
title: ¿Hachecincoqué? Contenidos interactivos en Aeducar, Moodle y WordPress
---

# ¿Hachecincoqué? Contenidos interactivos en Aeducar, Moodle y WordPress

>Introducción a la creación de contenido interactivo H5P y su aplicación en el aula.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>- ¿¿¿Hachecincoqué???
- Ventajas y desventajas
- Algunos tipos de contenidos y su aplicación en el aula
- Integración con Moodle/Aeducar
- Lumi App / Lumi Cloud y posibilidades de uso
- Banco de recursos
- Formación en Aularagón
- Práctica con algunas actividades
- Integración con WordPress

-   Público objetivo:

>Docentes de todos los niveles educativos con curiosidad por utilizar una herramienta de software libre para la presentación de contenidos y para la evaluación.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 17:00-17:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Héctor Banzo, Paola García

-   Bio:

>Héctor Banzo: Maestro de Inglés, actualmente es Asesor en Competencia Digital del Centro de Profesorado Ana Abarca de Bolea en Huesca y Asesor CIFPA en la provincia de Huesca. Colabora como tutor en Aularagon e imparte formación en diferentes áreas. Interesado en aprender en cualquier ámbito que tenga que ver con las TIC, TAP y TEP, actualmente está casi a tiempo completo enfocado en su paternidad.<br><br>
Paola García: Maestra de inglés. Ha sido tutora, especialista de inglés y directora del CEIP Infanta Elena de Utebo (Zaragoza)  y actualmente ocupa una de las mentorías digitales en el Centro de Profesorado Juan de Lanuza de Zaragoza, acompañando y asesorando  a los centros educativos para la elaboración de su Plan Digital de centro y en la formación de los docentes en competencia digital. Está interesada en el diseño,  le gustan los memes de gatitos y su sueño es completar algún día todos los cursos de Aularagon.


### Info personal:

-   Twitter: <https://twitter.com/@paolagarab>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
