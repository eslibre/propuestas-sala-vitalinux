---
layout: 2023/post
section: proposals
category: talks
author: Alberto Larraz Dalmases
title: IsardVDI&#58; la solución de virtualización de escritorios que contribuye a democratizar y facilitar el acceso al software necesario para cada acción educativa
---

# IsardVDI: la solución de virtualización de escritorios que contribuye a democratizar y facilitar el acceso al software necesario para cada acción educativa

>Desde un navegador accedes a tus escritorios de trabajo, con el sistema operativo y el software configurado con lo que necesitas para empezar a hacer tus trabajos de clase.  Esa experiencia es la que proporciona IsardVDI, que nació de la necesidad de simplificar la gestión y dar libertad a los docentes o gestores de tecnología para preparar un escritorio a la medida de cada usuario.<br><br>
IsardVDI pasa a ser una herramienta educativa a disposición de profesores y alumnos. Da nuevas posibilidades para crear prácticas, proyectos, laboratorios, aprender redes, integrar dispositivos... Rompe la barrera de tener que estar ligado a un sitio y pc en concreto, pudiendo montar aulas virtuales sin importar el PC desde donde te conectes, alargando la vida útil de los equipos.<br><br>
Actualmente damos soporte a más de 100 centros educativos del estado español. Explicaremos algunos casos de uso, destacando la parte de GPUs en los escritorios, y comentaremos que factores hay que tener en cuenta para que la implantación de este tipo de soluciones sea exitosa.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>El foco de la mayoria de las soluciones de VDI está en sectores empresariales, donde la creación y despliegue de escritorios suele caer en manos de un gestor o administrador de sistemas. IsardVDI pasa a ser una herramienta educativa a disposición del docente. Da nuevas posibilidades para crear prácticas, proyectos, laboratorios, aprender redes, integrar escritorios con equipos o dispositivos reales... Usar escritorios virtuales rompe las barreras de estar ligado a un sitio y pc en concreto, pudiendo montar aulas virtuales con distiintos tipos de ordenadores y alargando la vida útil de los equipos. La prioridad de IsardVDI siempre ha sido tener diferentes roles, con una gestión de permisos y quotas granular, que permitie a profesores y alumnos crear y compartir escritorios de forma autónoma.<br><br>
Mostraremos las posibilidades de la herramienta, desplegando un aula virtual y cómo acceden los usuarios con los diferentes tipos de visores de escritorio (integrados en el navegador, RDP y Spice).<br><br>
La solución de virtualización de escritorios pretende ser una alternativa a sistemas de VDI (Virtual Desktop Infraestructure) privativos y costosos  como Citrix o Vmware Horizon y está desarrollada a partir las tecnologías de virtualización de linux (libvirt-qemu-kvm). Compartiremos como el enfoque del desarrollo basado en contenedores nos ha permitido que una solución compleja se pueda desplegar con facilidad con más o menos volumen y número de servidroes. Explicaremos como puedes instalar IsardVDI en tu portátil, y cómo implantamos soluciones de miles de escritorios y usuarios.<br><br>
¿Y si necesitamos enseñar diseño 3D, aplicar texturas, hacer aniimaciones, diseñar piezas industriales, o entrenar modelos de inteligencia artificial? Todas estos ámbitos necesitan de equipos con bastante memoria, cores de CPU y sobre todo GPUs, que encarecen los equipos con un coste que no siempre nos podemos permitir en las aulas y en las casas, donde hay alumnos no pueden realizar esa inversión. Con IsardVDI podrás invertir en tarjetas profesionales GPU de NVIDIA y rentabilizar esa inversión repartiendo esa potencia gráfica entre los usuarios que lo necesiten, permitiendo que desde una raspberry-pi o un pc antiguo puedas, por ejemplo, mover o renderizar un modelo 3D.<br><br>
IsardVDI es un proyecto de software libre, con personas y organizaciones que lo están usando en diferentes lugares. Actualmente estamos dando soporte a más de 100 centros educativos del estado español. A partir de esta experiencia explicaremos qué factores hay que tener en cuenta para que la implantación sea exitosa y algunas de las diferentes modalidades de despliegue de la solución que puedes realiza, incluyendo la posibilidad de desplegar la solución en plataformas cloud.

-   Web del proyecto: <https://isardvdi.com>

-   Público objetivo:

>Gestores de TIC, sysadmins,  docentes, estudiantes, entusiastas del software libre.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 13:30-14:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Alberto Larraz Dalmases

-   Bio:

>Ingeniero de Telecomunicaciones, educador, sysadmin y programador. Co-fundador de IsardVDI. Gestionando una startup de soluciones de software libre.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@isard>
-   Twitter: <https://twitter.com/isard_vdi>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/isard/isardvdi/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
