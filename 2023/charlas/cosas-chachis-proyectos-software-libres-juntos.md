---
layout: 2023/post
section: proposals
category: talks
author: Sara Arjona Téllez
title: Cosas chachis que pasan cuando diversos proyectos de software libre se juntan&#58; integración de H5P, BBB y Matrix con Moodle
---

# Cosas chachis que pasan cuando diversos proyectos de software libre se juntan: integración de H5P, BBB y Matrix con Moodle

>La idea de la charla es compartir algunas de las experiencias de integración de Moodle, la plataforma de aprendizaje de software libre más utilizada en el mundo, con otros proyectos de código abierto.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Además de explicar cómo se han integrado dichos proyectos, también se mencionarán otros proyectos similares que también se integran, para poner en valor los beneficios que tiene el software libre.<br><br>
Moodle es una plataforma de aprendizaje de código abierto desarrollada en PHP, usada a nivel mundial por escuelas, universidades y empresas: <https://moodle.org/><br><br>
Desde 2002, año en que se publicó la primera versión, ha estado disponible como software abierto y, con el tiempo, se han ido integrando otras herramientas, para ofrecer más funcionalidades a los usuarios, sin tener que desarrollarlas desde cero.<br><br>
Así, por ejemplo, hace unos años, se incorporó la integración con H5P, una herramienta de software libre que permite crear contenido interactivo: <https://h5p.org/><br><br>
Recientemente también se ha integrado con BigBlueButton, otro proyecto de código abierto que se usa como sistema de conferencia web: <https://bigbluebutton.org/><br><br>
Y actualmente se está trabajando en la integración con Matrix, para mejorar el sistema de comunicación: <https://matrix.org/><br><br>
Estos son sólo algunos ejemplos que muestran algunos de los beneficios del software libre.

-   Web del proyecto: <https://moodle.org/>

-   Público objetivo:

>Cualquier persona interesada en saber más de la plataforma de aprendizaje Moodle y conocer un poco mejor algunos de los proyectos con los que se integra, como H5P, BigBlueButton o Matrix.<br><br>
Como principalmente se revisarán estas integraciones desde un punto de vista funcional, no está orientada a los perfiles más técnicos.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 17:30-18:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Sara Arjona Téllez

-   Bio:

>Soy ingeniera informática y trabajo en Moodle HQ como desarrolladora desde 2017, donde he participado en la implementación de diversos proyectos como la integración de las insignias con el estándar de IMS Open Badges y la herramienta de creación de contenido interactivo H5P, entre otros.<br><br>
Desde el 2003 y durante prácticamente 14 años colaboré en el desarrollo y mantenimiento de algunas de las herramientas que el Departament d'Educació de Catalunya ofrece al profesorado y estudiantado, como Ágora o XTECBlocs, basados en proyectos de código abierto, como Moodle o WordPress.

### Info personal:

-   Twitter: <https://twitter.com/sara_arjona>
-   GitLab (u otra forja) o portfolio general: <https://github.com/sarjona>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
